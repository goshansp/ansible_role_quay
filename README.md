# Ansible Role Template

Deploy Quay on CoreOS


# TODO: Manual PG Extension Enabling
```
podman exec -it postgresql-quay /bin/bash
psql -d quay -U postgres
CREATE EXTENSION IF NOT EXISTS pg_trgm;
SELECT * FROM pg_extension;
\q
exit
systemctl --user start quay.service
```


# Debug Notes
```
$ podman run -it --rm --network systemd-macvtap0-quay alpine ip addr
```


# Based upon
https://docs.projectquay.io/deploy_quay.html
